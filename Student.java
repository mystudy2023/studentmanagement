/*
编写学员实体类 Student，对应成员变量包含：学号 id、姓名 name、性别 sex；

        编写学员管理类 StudentManagement ，实现添加学员方法 addStudent()。

        编写StudentManagement的main方法进行学员信息的添加：

        学号：1001,姓名：张三,性别：男。
        学号：1002,姓名：莉丝,性别：女。
        学号：1003,姓名：王武,性别：男。
*/

public class Student {
    public int id;
    public String name;

    public String sex;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public void Student(int id, String name, String sex){

        this.id=id;
        this.name=name;
        this.sex=sex;
    }

}
